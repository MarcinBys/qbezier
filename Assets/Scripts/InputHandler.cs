﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InputHandler : MonoBehaviour {

    // <<< DRAWCURVE SCRIPT ACCESS >>>
    private DrawCurve drawCurve;

    // <<< INPUT FIELDS >>>
    public InputField setPx;
    public InputField setPy;
    public InputField lambdaValues;

    // <<< DISPLAY DATA TEXT >>>
    public Text controlPointsCounter;
    public Text lambdaCounter;

    // <<< BUTTONS >>>
    public Button AddButton;
    public Button EndButton;

    private void Start()
    {
        drawCurve = GetComponent<DrawCurve>();

        // Set InputFields not interactable
        lambdaValues.interactable = false;
        EndButton.interactable = false;
    }


    /// <summary>
    /// [Button function] Create new control point on scene at (setPx, setPy).
    /// </summary>
    public void AddControlPoint()
    {
        try
        {
            drawCurve.SpawnControlPoint(new Vector2(float.Parse(setPx.text, System.Globalization.CultureInfo.InvariantCulture), float.Parse(setPy.text, System.Globalization.CultureInfo.InvariantCulture)));
        }
        catch (Exception e)
        {
            Debug.LogException(e, this);
        }
        
        // Enable END Button when at least 2 Control Points are defined
        if (drawCurve.controlPoints.Count.Equals(2))
        {
            EndButton.interactable = true;
        }

        ClearInputFields();
        DisplayData();
    }


    /// <summary>
    /// [Button function] Disable position InputFields and call LoadLambdaValues() to lambdaValues InputField.
    /// </summary>
    public void EndAddingControlPoints()
    {
        LoadLambdaValues();     // need to be invoked only once in this script

        // Set InputFields interactable state accordingly
        setPx.interactable = false;
        setPy.interactable = false;
        lambdaValues.interactable = true;

        // Set Buttons interactable state accordingly
        AddButton.interactable = false;
        EndButton.interactable = false;

        // Start generating curve
        Generate();
    }


    /// <summary>
    /// [Button function] Clear setPx and setPy input fields.
    /// </summary>
    private void ClearInputFields()
    {
        setPx.text = "";
        setPy.text = "";
    }


    /// <summary>
    /// [Button function] Save lambdaValues input field to list. Lambda value, if as fraction, need to be defined as for ex. "0,5", not "0.5".
    /// </summary>
    public void SetLambdaValues()
    {
        drawCurve.lambda = new List<float> { 0, };          // 0 is not used by user
        
        string[] values = lambdaValues.text.Split(' ');     // split values separated by ","

        foreach (string value in values)
        {
            drawCurve.lambda.Add(float.Parse(value));        // add each value to list
        }
    }


    /// <summary>
    /// Load lambda list to lambdaValues input field.
    /// </summary>
    private void LoadLambdaValues()
    {
        // Create lambda list with 0 values
        drawCurve.CreateLambdaList();

        // Needed to control foreach loop
        int loopCounter = 0;
        int loopLastIndex = drawCurve.lambda.Count - 1;

        // Clear lambdaValues field
        lambdaValues.text = "";
        
        foreach (float value in drawCurve.lambda)
        {
            if (loopCounter.Equals(0))                      // skip first 0 value in lambda list
            {
                loopCounter++;
                continue;
            }
            else if (loopCounter.Equals(loopLastIndex))     // write last value in list to input field without comma
            {
                lambdaValues.text += value;
            }
            else
            {
                lambdaValues.text += value + " ";
            }

            loopCounter++;
        }
    }


    /// <summary>
    /// Shows how many control points has been spawned and how many lambdas can be set by user.
    /// </summary>
    private void DisplayData()
    {
        controlPointsCounter.text = drawCurve.controlPoints.Count.ToString();
        lambdaCounter.text = (drawCurve.controlPoints.Count - 1).ToString();
    }


    /// <summary>
    /// Start drawing Quasi Bezier curve based on created control points.
    /// </summary>
    public void Generate()
    {
        try
        {
            drawCurve.QuasiBezier();

            drawCurve.canBeDrawn = true;
        }
        catch (Exception e)
        {
            Debug.LogException(e, this);
        }
    }


    /// <summary>
    /// Clears (restarts) whole scene.
    /// </summary>
    public void RestartScene()
    {
        SceneManager.LoadScene(0);
    }
}