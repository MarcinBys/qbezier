﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawCurve : MonoBehaviour {

    // <<< LINE RENDERER COMPONENT ACCESS >>>
    private LineRenderer line;

    // <<< CONTROL POINT VARIABLES >>>
    public GameObject ControlPoint;
    public List<GameObject> controlPoints;

    // <<< QUASI BEZIER VARIABLES >>>
    public List<float> lambda;              // curve shape parameters list
    private int n;                          // polynomial degree
    private int nhalf;                      // [n/2]
    public bool canBeDrawn;                 // check if all variables are initialized


    /// <summary>
    /// Initialization
    /// </summary>
    private void Start ()
    {
        line = GetComponent<LineRenderer>();

        canBeDrawn = false;                 // set to false as all needed variables haven't been set yet
	}


    private void Update()
    {
        if (canBeDrawn)
        {
            QuasiBezier();
        }
    }


    /// <summary>
    /// Calculate Generalized Bernstein Polynomial.
    /// </summary>
    /// <param name="t">step</param>
    /// <param name="n">polynomial degree</param>
    /// <param name="i">loop iteration</param>
    /// <returns></returns>
    private float GeneralizedBernstein(int n, int i, float t)
    {
        float B;

        if (i == 0)
        {
            B = Mathf.Pow(1 - t, n - 1) * ((1 - t) - lambda[1] * t);
        }
        else if (i >= 1 && i <= nhalf - 1)
        {
            B = Mathf.Pow(t, i) * Mathf.Pow(1 - t, n - 1 - i) * (Newton(n, i) * (1 - t) + lambda[i] * (1 - t) - lambda[i + 1] * t);
        }
        else if (i == nhalf)
        {
            B = Mathf.Pow(t, nhalf) * Mathf.Pow(1 - t, n - 1 - nhalf) * (Newton(n, nhalf) * (1 - t) + lambda[nhalf] * (1 - t) + lambda[nhalf + 1] * t);
        }
        else if (i >= nhalf + 1 && i <= n - 1)
        {
            B = Mathf.Pow(t, i) * Mathf.Pow(1 - t, n - 1 - i) * (Newton(n, i) * (1 - t) - lambda[i] * (1 - t) + lambda[i + 1] * t);
        }
        else //if (i == n)
        {
            B = Mathf.Pow(t, n) * (1 - lambda[n]);
        }

        return B;
    }


    /// <summary>
    /// Calculate binomial theorem.
    /// </summary>
    /// <param name="n"></param>
    /// <param name="k"></param>
    /// <returns></returns>
    private long Newton(int n, int k)
    {
        long result = 1;
        int i;

        for (i = 1; i <= k; i++)
        {
            result = result * (n - i + 1) / i;
        }

        return result;
    }


    /// <summary>
    /// Calculate and draw Quasi Bezier curve (Q-Bezier).
    /// </summary>
    public void QuasiBezier()
    {
        // Clear table with coordinates of old rendered line (for drawing new one)
        line.positionCount = 0;
        line.positionCount += 1;

        // Calculate polynomial degree (-1 because we count from 0)
        n = controlPoints.Count - 1;

        // Vector with position coordinates to draw every step
        Vector2 P;

        // Starting position of curve drawing, at first control point position - terminal property P(0) = P0
        int position = 0;
        line.SetPosition(position, controlPoints[0].transform.position);
        position++;
        

        // Calculate [n/2] and save it to nhalf variable
        if (n % 2 == 1)     // when polynomial degree is odd
        {
            nhalf = (n + 1) / 2;
        }
        else                // when polynomial degree is even
        {
            nhalf = n / 2;
        }


        // P(t) = sum(i=0, n) { P[i] * B_in(t) } for t in interval [0, 1]
        for (float t = 0; t <= 1; t += 0.01f)   // t - step
        {
            P = new Vector2(0, 0);

            for (int i = 0; i <= n; i++)
            {
                float generalizedBernstein = GeneralizedBernstein(n, i, t);             // compute GeneralizedBernstein
                P.x += controlPoints[i].transform.position.x * generalizedBernstein;    // compute x coordinate of point P
                P.y += controlPoints[i].transform.position.y * generalizedBernstein;    // compute y coordinate of point P
            }

            // Draw line to new point P
            line.positionCount += 1;        // add slot in LineRenderer's array for a new vector
            line.SetPosition(position, P);  // set position as new point P
            position++;                     // increase position index
        }

        // draw line to last control point if terminal property (   P(1) = Pn, if lambda[n] = 0   ) is fulfilled
        if (lambda[n].Equals(0))
        {
            line.positionCount += 1;
            line.SetPosition(position, controlPoints[controlPoints.Count - 1].transform.position);
        }
    }


    /// <summary>
    /// Spawn new control point on the scene at given position.
    /// </summary>
    /// <param name="position"></param>
    public void SpawnControlPoint(Vector2 position)
    {
        controlPoints.Add(Instantiate(ControlPoint, position, Quaternion.identity));
    }


    /// <summary>
    /// Creates shape parameter list with each parameter = 0, based on how many control points has been added to scene.
    /// </summary>
    public void CreateLambdaList()
    {
        lambda = new List<float> { 0, };    // first element is not used, but needed to function correctly for GeneralizedBernstein formula, not visible to user

        for (int i = 1; i <= controlPoints.Count - 1; i++)      // add lambda values of 0
        {
            lambda.Add(0);
        }
    }
}